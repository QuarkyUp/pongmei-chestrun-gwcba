#include <GWCA/Constants/Constants.h>
#include <GWCA/GWCA.h>
#include <GWCA/Utilities/Hooker.h>
#include <Windows.h>
#include <gwcba/inventory.h>
#include <pongmei.h>

#include <cstdlib>

DWORD __stdcall ThreadEntry(HMODULE hModule) {
  GW::HookBase::Initialize();
  if (!GW::Initialize()) {
    FreeLibraryAndExitThread(hModule, EXIT_SUCCESS);
    return EXIT_SUCCESS;
  }
  GW::HookBase::EnableHooks();

  run();

  std::chrono::milliseconds timespan(100);
  std::this_thread::sleep_for(timespan);

  GW::DisableHooks();

  while (GW::HookBase::GetInHookCount()) Sleep(16);
  Sleep(16);

  GW::Terminate();
  FreeLibraryAndExitThread(hModule, EXIT_SUCCESS);
}

DWORD __stdcall SafeThreadEntry(HMODULE hModule) {
  __try {
    return ThreadEntry(hModule);
  } __except (EXCEPTION_CONTINUE_SEARCH) {
    return EXIT_SUCCESS;
  }
}

DWORD WINAPI init(HMODULE hModule) {
  __try {
    GW::Scanner::Initialize();
    auto **found = (DWORD **)GW::Scanner::Find("\xA3\x00\x00\x00\x00\xFF\x75\x0C\xC7\x05", "x????xxxxx", +1);
    if (!(found && *found)) {
      MessageBoxA(nullptr, "No Logged In Character.", "DLL Error", 0);
      FreeLibraryAndExitThread(hModule, EXIT_SUCCESS);
    }
    DWORD *is_ingame = *found;
    while (*is_ingame == 0) {
      Sleep(100);
    }
    SafeThreadEntry(hModule);
  } __except (EXCEPTION_CONTINUE_SEARCH) {
  }

  return 0;
}

BOOL WINAPI DllMain(_In_ HMODULE _HDllHandle, _In_ DWORD _Reason, _In_opt_ LPVOID _Reserved) {
  DisableThreadLibraryCalls(_HDllHandle);
  if (_Reason == DLL_PROCESS_ATTACH) {
    __try {
      HANDLE hThread = CreateThread(nullptr, 0, (LPTHREAD_START_ROUTINE)init, _HDllHandle, 0, nullptr);
      if (hThread != nullptr) CloseHandle(hThread);
    } __except (EXCEPTION_CONTINUE_SEARCH) {
    }
  }
  return TRUE;
}