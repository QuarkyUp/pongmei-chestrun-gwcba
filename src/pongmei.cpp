#include <gwcba/agents.h>
#include <gwcba/bot.h>
#include <gwcba/gwcba.h>
#include <gwcba/movement.h>
#include <gwcba/travel.h>
#include <pongmei.h>

#include <functional>
#include <vector>

bool run() {
  if (GW::Map::GetMapID() != GW::Constants::MapID::Boreas_Seabed_outpost_mission)
    mapTravelRandom(GW::Constants::MapID::Boreas_Seabed_outpost_mission, false);

  std::map<GW::Constants::MapID, const std::function<bool(void)>> logicOnMap{
      {GW::Constants::MapID::Boreas_Seabed_outpost_mission, boreasSeabedOutpostRoutine},
      {GW::Constants::MapID::Pongmei_Valley, pongmeiValleyExplorableRoutine}};

  botRunning = true;
  runLogicOnMapLoading(logicOnMap);

  return true;
}

bool boreasSeabedOutpostRoutine() {
  GW::PartyMgr::SetHardMode(false);
  moveTo(GW::GamePos(-25773.07, 2004.78, 0));
  moveTo(GW::GamePos(-25985.89, 2505.43, 0));
  moveToZone(GW::GamePos(-26006.47, 2646.29, 0), GW::Constants::MapID::Pongmei_Valley);
  return true;
}

bool pongmeiValleyExplorableRoutine() {
  waitForMapLoaded(GW::Constants::MapID::Pongmei_Valley);

  std::vector<GW::AgentID> openedChestIds = {};

  std::function<bool()> openChestsInCompass = [&openedChestIds]() {
    std::vector<GW::AgentGadget *> chests = getGadgetsInRangeOfAgent();
    for (const auto &item : chests) {
      if ((36 < item->height1 && item->height1 < 37) && (34 < item->width1 && item->width1 < 35)) {
        if (std::find(openedChestIds.begin(), openedChestIds.end(), item->agent_id) == openedChestIds.end()) {
          GW::Agents::GoSignpost(item);
          GW::Items::OpenLockedChest();
          openedChestIds.push_back(item->agent_id);
        }
      }
    }

    return true;
  };

  std::vector<GW::GamePos> waypoints = {
      GW::GamePos(20831.08, 2218.37, 0),  GW::GamePos(18024.83, 1017.97, 0),  GW::GamePos(15545.08, -2835.77, 0), GW::GamePos(16078.55, -4233.35, 0),
      GW::GamePos(15310.21, -2704.64, 0), GW::GamePos(13512.00, -1930.63, 0), GW::GamePos(13293.83, 2036.50, 0),  GW::GamePos(10798.29, 3759.91, 0),
      GW::GamePos(10029.80, 4421.54, 0),  GW::GamePos(9962.42, 8113.25, 0),   GW::GamePos(9937.50, 10210.47, 0),  GW::GamePos(7427.72, 7003.56, 0),
      GW::GamePos(3434.16, 6160.64, 0),   GW::GamePos(-1786.99, 6321.94, 0),  GW::GamePos(-4083.03, 6915.73, 0),  GW::GamePos(-6115.06, 6974.51, 0),
      GW::GamePos(-8780.82, 4699.88, 0),  GW::GamePos(-11188.42, 5625.52, 0), GW::GamePos(-14728.43, 5986.89, 0), GW::GamePos(-14526.26, 2164.19, 0),
      GW::GamePos(-14449.81, -400.58, 0), GW::GamePos(-13698.99, -4760.16, 0)};

  for (const auto &waypoint : waypoints)
    if (!moveTo(waypoint, openChestsInCompass)) return false;

  mapTravelRandom(GW::Constants::MapID::Boreas_Seabed_outpost_mission, false);

  return true;
}
