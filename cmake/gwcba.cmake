include_guard()
include(FetchContent)

FetchContent_Declare(
        gwcba
        GIT_REPOSITORY https://gitlab.com/QuarkyUp/gw-client-bot-api.git
        GIT_TAG e980341354797b053ae0b01d0032d9ede9c16154
)
FetchContent_MakeAvailable(gwcba)
